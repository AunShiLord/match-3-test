﻿

public struct MatrixIndex
{
    public int x, y;

    public MatrixIndex(int _x, int _y)
    {
        x = _x;
        y = _y;
    }
}
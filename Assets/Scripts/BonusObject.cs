﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnMouseDown()
    {
        AudioManager.instance.Play(EnumSounds.BONUS);
        GameField.instance.RemoveObjectsOnLine(GameField.instance.playfroundHeight - 1);
        // this object was clicked - do something
        Destroy(gameObject);
    }
}

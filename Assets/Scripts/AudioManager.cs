﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public Sound[] sounds;

    public static AudioManager instance;

	// Use this for initialization
	void Awake () {
		if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.clip;
            s.audioSource.volume = s.volume;
            s.audioSource.pitch = s.pitch;
            s.audioSource.loop = s.loop;
        }
	}

    void Start()
    {
        Play(EnumSounds.MAIN_THEME);
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s != null)
        {
            s.audioSource.Play();
        }
    }

    public void Play(EnumSounds sound)
    {
        switch (sound)
        {
            case EnumSounds.MAIN_THEME:
                Play("MainTheme"); break;
            case EnumSounds.EXPLODE:
                Play("Explode"); break;
            case EnumSounds.BONUS:
                Play("Bonus"); break;
            default:
                break;
        }
    }
}

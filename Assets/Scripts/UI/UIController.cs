﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public ColumnButton[] buttons;
    public Text scoreText;
    public PlayPauseButton playPauseButton;
    public Text pauseText;

    private bool isPaused = false;
    private bool isGameOver = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // set buttons interactable by indexes
    public void UpdateButtonsState(List<int> activeButtonIndexes)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            if (activeButtonIndexes.Contains(i))
            {
                buttons[i].buttonComponent.interactable = true;
            }
            else
            {
                buttons[i].buttonComponent.interactable = false;
            }
        }
    }

    public void UpdateScoreText(int score)
    {
        scoreText.text = "Score: " + score;
    }

    public void Restart()
    {
        isGameOver = false;
        pauseText.gameObject.SetActive(false);
    }

    public void PlayPause()
    {
        if (!isGameOver)
        {
            isPaused = !isPaused;
            Time.timeScale = isPaused ? 0 : 1;
            pauseText.text = "PAUSE";
            pauseText.gameObject.SetActive(isPaused);
            playPauseButton.UpdateButton(isPaused);
        }
    }

    public void GameOver(int score)
    {
        isGameOver = true;
        {
            pauseText.gameObject.SetActive(true);
            pauseText.text = "GAME OVER! \n Score: " + score;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayPauseButton : MonoBehaviour {

    public UIController uiController;
    public Image buttonCurrentImage;
    public Sprite playSprite;
    public Sprite pauseSprite;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnButtonClick()
    {
        uiController.PlayPause();
    }

    public void UpdateButton(bool isPaused)
    {
        if (isPaused)
        {
            buttonCurrentImage.sprite = playSprite;
            Time.timeScale = 0;
        }
        else
        {
            buttonCurrentImage.sprite = pauseSprite;
            Time.timeScale = 1;
        }
    }
}

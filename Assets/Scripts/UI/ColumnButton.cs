﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColumnButton : MonoBehaviour {

    public int columnIndex = 0;
    public Button buttonComponent;
    
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnButtonPress()
    {
        GameField.instance.ChangeActiveObjectPosition(columnIndex);
    }
}

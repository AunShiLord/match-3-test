﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public Vector3 axis;
    public float defaultSpeed = 5f;
    public float speed = 20.0f;

    public Transform lookAt;
    public Transform rotateAroundPoint;
    /*
    public float maxZoomDistance = 0f;
    public float minZoomDistance = 0f;

    public float forwardSpeed = 0f;
    */

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

       
        transform.RotateAround(rotateAroundPoint.position, axis, Time.deltaTime * speed);
        if (lookAt != null)
        {
            transform.LookAt(lookAt);
        }

        /*
        if (forwardSpeed != 0)
        {
            float distance = Vector3.Distance(lookAt.transform.position, transform.position + transform.forward * forwardSpeed);
            if (distance > maxZoomDistance && minZoomDistance > distance)
            {
                transform.position += transform.forward * forwardSpeed;
            }
        }
        */

    }

    void ShakeCamera()
    {

    }
}

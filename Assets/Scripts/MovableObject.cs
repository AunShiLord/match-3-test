﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObject : MonoBehaviour {

    public EnumObjectType type;
    public bool isSelected = false;
    public Vector3 destinationPoint;
    public Animator animator;
    public EnumObjectState state
    {
        get { return _state; }
    }

    private float speed = .75f;
    public GameObject halo;

    public MatrixIndex playgroundCoordinate = new MatrixIndex(-1, -1);
    private float swapingSpeedMultiplyer = 4f;
    private EnumObjectState _state = EnumObjectState.STATIONARY;

    private void Awake()
    {
        destinationPoint = transform.position;
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {

        // if object stationary - it can't be active
        if (state == EnumObjectState.STATIONARY)
        {
            SetSelected(false);
        }
	}

    public void SetNewDestinationPoint(Vector3 point)
    {
        destinationPoint = point;
    }

    // return true if move is over
    public bool Move()
    {
        float speedMuliplyer = 0;

        switch (state)
        {
            case EnumObjectState.MOVING:
                speedMuliplyer = 1; break;
            case EnumObjectState.SWAPING:
            case EnumObjectState.FALLING:
                speedMuliplyer = swapingSpeedMultiplyer; break;
            default:
                break;
        }

        if (state == EnumObjectState.MOVING || state == EnumObjectState.SWAPING || state == EnumObjectState.FALLING)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, destinationPoint, Time.deltaTime * speed * speedMuliplyer);
        }

        // if movement is over, changing state to Stationary
        if (transform.localPosition == destinationPoint)
        {
            SetNewState(EnumObjectState.STATIONARY);
            return true;
        }
        return false;
    }

    public void UpdatePlaygroundCoordinate(MatrixIndex index)
    {
        playgroundCoordinate = index;
    }

    public void SetSelected(bool _isSelected)
    {
        isSelected = _isSelected;
        halo.SetActive(_isSelected);
    }

    public void SetNewState(EnumObjectState newState)
    {
        _state = newState;
    }

    public void Disappear()
    {
        animator.SetTrigger("Disappear");
    }
}

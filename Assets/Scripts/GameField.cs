﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameField : MonoBehaviour {

    public static GameField instance;
    public UIController uiController;
    public Vector3 startPoint;
    public float objectsOffset;
    public GameObject[] possibleObjects;
    public GameObject bonusPrefab;
    public int playgroundWidth = 5;
    public int playfroundHeight = 7;
    public int minStreak = 3;
    public int bonusChance = 3;
    public bool debugMode = true;

    private GameObject[,] gameMatrix;
    private List<GameObject> spawnedGameObjects = new List<GameObject>();
    private MovableObject activeObject;
    private List<GameObject> bonuses = new List<GameObject>();
    private int score = 0;

    public Text debugText;

	// Use this for initialization
	void Start () {
        if (instance == null)
        { 
            instance = this;
        }
        else if (instance == this)
        { 
            Destroy(gameObject);
        }

        gameMatrix = new GameObject[playgroundWidth, playfroundHeight];
        SpawnFirstLine();
        StartCoroutine(SpawnObjects(3));
        StartCoroutine(SpawnBonus());
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Restart();
        }

        MovePhase();
        MatchPhase();
        UpdateActiveObjectPosiblePositions();
        uiController.UpdateScoreText(score);

        if (debugText != null && debugMode)
        {
            debugText.text = Print();
        }
	}

    public void Restart()
    {     
        Time.timeScale = 1;
        uiController.Restart();
        foreach (GameObject bonus in bonuses)
        {
            if (bonus != null)
            {
                Destroy(bonus);
            }
        }

        StopAllCoroutines();
        ClearLevel();

        score = 0;

        SpawnFirstLine();
      
        StartCoroutine(SpawnObjects(3));
        StartCoroutine(SpawnBonus());
    }

    private string Print()
    {
        string output = "";
        for (int y = 0; y < playfroundHeight; y++)
        {
            output += "| ";
            for (int x = 0; x < playgroundWidth; x++)
            {
                if (gameMatrix[x,y] == null)
                {
                    output += " _ ";
                }
                else if (gameMatrix[x, y].GetComponent<MovableObject>().state == EnumObjectState.MOVING)
                {
                    output += " M ";
                }
                else if (gameMatrix[x, y].GetComponent<MovableObject>().state == EnumObjectState.STATIONARY)
                {
                    output += " S ";
                }
                else
                {
                    output += " O ";
                }
            }

            output += " |\n";
        }
        return output;
    }

    private void MovePhase()
    {
        // moving all objects in movement state and ckecking if Stationary objects can move down
        // Thats why checking from the bottom of the matrix.
        for (int y = playfroundHeight - 1; y >= 0; y--)
        {
            for (int x = 0; x < playgroundWidth; x++)
            {

                if (gameMatrix[x,y] != null)
                {
                    MovableObject movable = gameMatrix[x, y].GetComponent<MovableObject>();
                    EnumObjectState currentState = movable.state;
                    if (movable.state == EnumObjectState.MOVING || movable.state == EnumObjectState.FALLING)
                    {
                        // if object after Move finishes and changes it's state to Statinary
                        // then we can update matrix and check if object will move further at the next frame
                        if (movable.Move())
                        {
                            if (y + 1 < playfroundHeight)
                            {
                                TryToMoveObjectDown(movable, x, y + 1, false, currentState);
                                gameMatrix[x, y + 1] = gameMatrix[x, y];
                            }
                            gameMatrix[x, y] = null;
                            movable.UpdatePlaygroundCoordinate(new MatrixIndex(x, y + 1));
                        }
                    }
                    else if (movable.state == EnumObjectState.SWAPING)
                    {
                        if (movable.Move())
                        {
                            MatrixIndex index = TransformVector3ToCoordinate(movable.gameObject.transform.localPosition);
                            if (y + 1 < playfroundHeight)
                            {
                                TryToMoveObjectDown(movable, index.x, index.y + 1, false, EnumObjectState.MOVING);
                            }
                            gameMatrix[index.x, index.y] = gameMatrix[x, y];
                            gameMatrix[x, y] = null;
                            movable.UpdatePlaygroundCoordinate(new MatrixIndex(index.x, index.y));
                        }
                    }
                    else if (movable.state == EnumObjectState.STATIONARY)
                    {
                        EnumObjectState state = EnumObjectState.FALLING;
                        foreach (GameObject go in spawnedGameObjects)
                        {
                            if (go == movable.gameObject)
                            {
                                state = EnumObjectState.MOVING;
                                break;
                            }
                        }
                        TryToMoveObjectDown(movable, x, y, true, state);
                    }
                }

            }
        }
        
    }

    private void MatchPhase()
    {
        List<MovableObject> objectsToDestroy = new List<MovableObject>();

        // first checking all horizontal matches
        for (int y = 0; y < playfroundHeight; y++)
        {
            int streak = 1;
            for (int x = 1; x < playgroundWidth; x++)
            {
                if (gameMatrix[x, y] != null && gameMatrix[x - 1, y] != null
                    && gameMatrix[x, y].GetComponent<MovableObject>().state == EnumObjectState.STATIONARY
                    && gameMatrix[x - 1, y].GetComponent<MovableObject>().state == EnumObjectState.STATIONARY
                    && gameMatrix[x - 1, y].GetComponent<MovableObject>().type == gameMatrix[x, y].GetComponent<MovableObject>().type)
                {
                    streak++;
                }
                else
                {
                    if (streak >= minStreak)
                    {
                        for (int i = x - streak; i < x; i++)
                        {
                            objectsToDestroy.Add(gameMatrix[i, y].GetComponent<MovableObject>());
                        }
                    }
                    streak = 1;
                }
            }

            if (streak >= minStreak)
            {
                for (int i = playgroundWidth - streak; i < playgroundWidth; i++)
                {
                    objectsToDestroy.Add(gameMatrix[i, y].GetComponent<MovableObject>());
                }
            }
        }


        // than checking all vertical matches
        for (int x = 0; x < playgroundWidth; x++)
        {
            int streak = 1;
            for (int y = 1; y < playfroundHeight; y++)
            {
                if (gameMatrix[x, y] != null && gameMatrix[x, y - 1] != null
                    && gameMatrix[x, y].GetComponent<MovableObject>().state == EnumObjectState.STATIONARY
                    && gameMatrix[x, y - 1].GetComponent<MovableObject>().state == EnumObjectState.STATIONARY
                    && (gameMatrix[x, y - 1].GetComponent<MovableObject>().type == gameMatrix[x, y].GetComponent<MovableObject>().type))
                {
                    streak++;
                }
                else
                {
                    if (streak >= minStreak)
                    {
                        for (int i = y - streak; i < y; i++)
                        {
                            objectsToDestroy.Add(gameMatrix[x, i].GetComponent<MovableObject>());
                        }
                    }
                    streak = 1;
                }
            }

            if (streak >= minStreak)
            {
                for (int i = playfroundHeight - streak; i < playfroundHeight; i++)
                {
                    objectsToDestroy.Add(gameMatrix[x, i].GetComponent<MovableObject>());
                }
            }
        }

        // updating score
        int tempScore = 0;
        if (objectsToDestroy.Count >= minStreak)
        {
            tempScore = 10;
        }
        if (objectsToDestroy.Count - minStreak > 0)
        {
            tempScore += (objectsToDestroy.Count - minStreak) * 4;
        }
        score += tempScore;

        // play sound
        if (objectsToDestroy.Count > 0)
        {
            AudioManager.instance.Play(EnumSounds.EXPLODE);
        }

        // now tagging objects to destroy
        foreach(MovableObject mgo in objectsToDestroy)
        {
            mgo.SetNewState(EnumObjectState.DISAPPEARING);
            mgo.Disappear();
            StartCoroutine(DestroyAfterTimeCoroutine(mgo, 0.5f));
        }
    }

    private void UpdateActiveObjectPosiblePositions()
    {
        List<int> emptyColums = new List<int>();

        // adding colums that on the same level with activeObject
        if (activeObject != null && activeObject.state == EnumObjectState.MOVING)
        {
            for (int x = 0; x < playgroundWidth; x++)
            {
                if (activeObject.playgroundCoordinate.y + 1 < playfroundHeight 
                    && gameMatrix[x, activeObject.playgroundCoordinate.y] == null)
                {
                    emptyColums.Add(x);
                }
            }
        }

        // also we can place object where another spawned objects falling EXCLUDING self
        foreach (GameObject go in spawnedGameObjects)
        {
            if (go != null
                && activeObject != null
                && go.GetComponent<MovableObject>().state == EnumObjectState.MOVING
                && go != activeObject.gameObject)
            {
                emptyColums.Add(go.GetComponent<MovableObject>().playgroundCoordinate.x);
            }
        }
        uiController.UpdateButtonsState(emptyColums);
    }

    private IEnumerator SpawnObjects(int numberOfObjects)
    {
        while (true)
        {
            SpawnNewObjects(numberOfObjects);
            yield return new WaitForSeconds(5f);
        }
    }

    private IEnumerator SpawnBonus()
    {
        while (true)
        {
            SpawnNewBonus();
            yield return new WaitForSeconds(1f);
        }
    }

    private void SpawnFirstLine()
    {
        GameObject leftObject = null;
        GameObject leftLeftObject = null;
        for (int i = 0; i < playgroundWidth; i++)
        {
            gameMatrix[i, playfroundHeight - 1] = CreateObjectOnCoordinate(i, playfroundHeight - 1);
            if (leftObject != null && leftLeftObject != null)
            {
                if (leftObject.GetComponent<MovableObject>().type == leftLeftObject.GetComponent<MovableObject>().type)
                {
                    while (gameMatrix[i, playfroundHeight - 1].GetComponent<MovableObject>().type == leftObject.GetComponent<MovableObject>().type)
                    {
                        Destroy(gameMatrix[i, playfroundHeight - 1]);
                        gameMatrix[i, playfroundHeight - 1] = CreateObjectOnCoordinate(i, playfroundHeight - 1);
                    }
                }
                
            }

            leftLeftObject = leftObject;
            leftObject = gameMatrix[i, playfroundHeight - 1];
            leftObject.GetComponent<MovableObject>().UpdatePlaygroundCoordinate(new MatrixIndex(i, playfroundHeight - 1));
        }
    }

    private void ClearLevel()
    {
        for (int x = 0; x < playgroundWidth; x++)
        {
            for (int y = 0; y < playfroundHeight; y++)
            {
                if (gameMatrix[x,y] != null)
                {
                    Destroy(gameMatrix[x, y]);
                    RemoveObjectOnCoordinate(x, y);
                }
            }
        }
    }

    private GameObject CreateObjectOnCoordinate(int x, int y)
    {
        Vector3 position = TransformCoordinateToVector3(x, y);
        GameObject randomPrefab = possibleObjects[Random.Range(0, possibleObjects.Length)];
        GameObject newObject = Instantiate(randomPrefab, gameObject.transform);
        newObject.GetComponent<MovableObject>().UpdatePlaygroundCoordinate(new MatrixIndex(x, y));
        newObject.transform.localPosition = position;
        return newObject;
    }

    private Vector3 TransformCoordinateToVector3(int x, int y)
    {
        return startPoint + new Vector3(x * objectsOffset, (playfroundHeight - 1 - y) * objectsOffset, 0);
    }

    private MatrixIndex TransformVector3ToCoordinate(Vector3 localPosition)
    {
        int x = (int)((localPosition.x - startPoint.x) / objectsOffset);
        int y = (int)(playfroundHeight - 1 - ((localPosition.y - startPoint.y) / objectsOffset));
        return new MatrixIndex(x, y);
    }

    private void SpawnNewObjects(int count)
    {
        // correcting number of objects to spawn
        if (count > playgroundWidth)
        {
            count = playgroundWidth;
        }

        // counting how many empty cells are available to spawn
        int emptyCount = 0;
        List<MatrixIndex> emptyGameObjects = new List<MatrixIndex>();
        // getting count of empty objects above
        for(int i = 0; i < playgroundWidth; i++)
        {
            if (gameMatrix[i, 0] == null)
            {
                emptyCount++;
                emptyGameObjects.Add(new MatrixIndex(i, 0));
            }
        }

        spawnedGameObjects.Clear();

        // spawning objects randomly
        if (!CheckGameOver(emptyCount))
        {
            for (int i = 0; i < count; i++)
            {
                int randomIndex = Random.Range(0, emptyGameObjects.Count);
                int x = emptyGameObjects[randomIndex].x;
                int y = emptyGameObjects[randomIndex].y;

                var newObject = CreateObjectOnCoordinate(x, y);
                gameMatrix[x, y] = newObject;
                spawnedGameObjects.Add(newObject);
                MatrixIndex destinationCoordinate = FindSutableCoordinateBellow(x, y);
                SetDestinationCoordinate(newObject, destinationCoordinate.x, destinationCoordinate.y);

                emptyGameObjects.RemoveAt(randomIndex);

                emptyCount--;
                if (CheckGameOver(emptyCount))
                {
                    GameOver();
                    break;
                }
            }
        }
        else
        {
            GameOver();
        }

        // randomly choosing active object
        if (spawnedGameObjects.Count > 0)
        {
            activeObject = spawnedGameObjects[Random.Range(0, spawnedGameObjects.Count)].GetComponent<MovableObject>();
            activeObject.SetSelected(true);
        }

    }

    private void SpawnNewBonus()
    {
        // getting chane of bonus
        if (Random.Range(0, 100) < bonusChance)
        {
            var stationaryObjects = GetAllStationaryObjects();
            if (stationaryObjects.Count > 0)
            {
                MovableObject bonusedObject = stationaryObjects[Random.Range(0, stationaryObjects.Count)].GetComponent<MovableObject>();
                Vector3 bonusPosition = TransformCoordinateToVector3(bonusedObject.playgroundCoordinate.x, bonusedObject.playgroundCoordinate.y);
                bonusPosition.z -= 0.5f;
                GameObject bonus = Instantiate(bonusPrefab, transform);
                bonus.transform.localPosition = bonusPosition;
                bonuses.Add(bonus);
            }
            
        }
    }

    private List<GameObject> GetAllStationaryObjects()
    {
        List<GameObject> stationaryObjects = new List<GameObject>();
        for (int x = 0; x < playgroundWidth; x++)
        {
            for (int y = 0; y < playfroundHeight; y++)
            {
                if (gameMatrix[x,y] != null
                    && gameMatrix[x, y].GetComponent<MovableObject>().state == EnumObjectState.STATIONARY)
                {
                    stationaryObjects.Add(gameMatrix[x, y]);
                }
            }
        }
        return stationaryObjects;
    }

    // checks if there is a place to spawn new objects. Else it is Game Over
    private bool CheckGameOver(int emptyPlaces)
    {
        // GAME OVER condition
        if (emptyPlaces <= 0)
        {
            // GAME OVER
            return true;
        }
        return false;
    }

    private void GameOver()
    {
        uiController.GameOver(score);
        StopAllCoroutines();
    }

    private MatrixIndex FindSutableCoordinateBellow(int x, int y)
    {
        if (y + 1 < playfroundHeight
            && gameMatrix[x, y + 1] == null)
        {
            return new MatrixIndex(x, y + 1);
        }
        return new MatrixIndex(x, y);
    }

    private void SetDestinationCoordinate(GameObject go, int x, int y)
    {
        if (gameMatrix[x,y] == null)
        {
            MovableObject movable = go.GetComponent<MovableObject>();
            if (movable != null)
            {
                Vector3 destination = TransformCoordinateToVector3(x, y);
                movable.SetNewDestinationPoint(destination);
            }
        }
    }

    private bool TryToMoveObjectDown(MovableObject movable, int x, int y, bool shouldMove, EnumObjectState state)
    {
        MatrixIndex index = FindSutableCoordinateBellow(x, y);
        if (index.y != y)
        {
            SetDestinationCoordinate(movable.gameObject, index.x, index.y);
            if (shouldMove)
            {
                movable.Move();
            }
            movable.SetNewState(state);
            return true;
        }
        if (movable.isSelected)
        {
            movable.SetSelected(false);
            activeObject = null;
        }
        return false;
    }

    public void RemoveObjectsOnLine(int lineIndex)
    {
        if (lineIndex < playfroundHeight)
        {
            for (int x = 0; x < playgroundWidth; x++)
            {
                if (gameMatrix[x, lineIndex] != null)
                {
                    // SOME MAGIC WITH REMOVABLE OBJECT
                    var mgo = gameMatrix[x, lineIndex].GetComponent<MovableObject>();
                    mgo.SetNewState(EnumObjectState.DISAPPEARING);
                    mgo.Disappear();
                    StartCoroutine(DestroyAfterTimeCoroutine(mgo, 0.5f));
                }
            }
        }
        
    }

    public void RemoveObjectOnCoordinate(int x, int y)
    {
        if (x < playgroundWidth && y < playfroundHeight
            && x >=0 && y >= 0)
        {
            gameMatrix[x, y] = null;
        }
        else
        {
            Debug.LogWarning("OBJECT WAS NOT NULLIFIED ON COORD: " + x + " " + y);
        }
        
    }

    public void ChangeActiveObjectPosition(int columnIndex)
    {
        if (activeObject != null && activeObject.state == EnumObjectState.MOVING)
        {
            // check if selected column is for another spawned object
            foreach (GameObject go in spawnedGameObjects)
            {
                if (go != null)
                {
                    MovableObject mog = go.GetComponent<MovableObject>();
                    if (activeObject != go
                        && mog.playgroundCoordinate.x == columnIndex)
                    {
                        mog.SetNewState(EnumObjectState.SWAPING);
                        SetDestinationCoordinate(mog.gameObject, activeObject.playgroundCoordinate.x, activeObject.playgroundCoordinate.y + 1);

                        activeObject.SetNewState(EnumObjectState.SWAPING);
                        SetDestinationCoordinate(activeObject.gameObject, columnIndex, activeObject.playgroundCoordinate.y + 1);
                    }
                } 
            }
            if (gameMatrix[columnIndex, activeObject.playgroundCoordinate.y] == null)
            {
                activeObject.SetNewState(EnumObjectState.SWAPING);
                SetDestinationCoordinate(activeObject.gameObject, columnIndex, activeObject.playgroundCoordinate.y + 1);
            }
        }
    }

    private IEnumerator DestroyAfterTimeCoroutine(MovableObject mgo, float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(mgo.gameObject);
        RemoveObjectOnCoordinate(mgo.playgroundCoordinate.x, mgo.playgroundCoordinate.y);
    }

}
